//Import react Components
import {useState, useContext, useEffect} from 'react';

//Import react-bootstrap Components
import {Row} from 'react-bootstrap';

//Import UserContext
import UserContext from '../userContext';

//Import Components
import ProductCard from '../components/ProductCard'
import AdminDashboard from '../components/AdminDashboard';


export default function Products(){

	const {user} = useContext(UserContext);


	//Input States
	const [productsArray, setProductsArray] = useState([])


	useEffect(()=>{


		fetch("https://floating-waters-44436.herokuapp.com/products/active")
		.then(res => res.json())
		.then(data => {


			setProductsArray(data.map(product => {

				return (

						<ProductCard key={product._id} productProp={product} />

					)

			}))

		})


	},[])



	return(

		user.isAdmin
		?
		<AdminDashboard />
		:
		<>
			<h1 className="text-center my-5">Find your scent!</h1>
			<Row>
				{productsArray}
			</Row>
		</>

	)
}