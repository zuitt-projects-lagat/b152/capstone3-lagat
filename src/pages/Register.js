//Import react Components
import {useState, useContext, useEffect} from 'react';

//Import react-bootstrap Components
import {Form, Button} from 'react-bootstrap';

//Import react-router-dom Components
import {Navigate} from 'react-router-dom';

//Import sweetalert2 Components
import Swal from 'sweetalert2';

//Import UserContext
import UserContext from '../userContext';


export default function Register(){


	const {user} = useContext(UserContext);


	//Input States
	const [firstName,setFirstName] = useState("");
	const [lastName,setLastName] = useState("");
	const [email,setEmail] = useState("");
	const [mobileNo,setMobileNo] = useState("");
	const [password,setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");


	//Conditional Rendering for Button
	const [isActive,setIsActive] = useState(false);

	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !=="" && confirmPassword !=="") 
			&& (password === confirmPassword) 
			&& (mobileNo.length === 11)){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[firstName,lastName,email,mobileNo,password,confirmPassword])



	function registerUser(e){


		e.preventDefault();


		fetch('https://floating-waters-44436.herokuapp.com/users/', {

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password,
				mobileNo: mobileNo

			})
		})
		.then(res => res.json())
		.then(data => {

			//console.log(data);


			if(data.email){

				Swal.fire({

					icon:"success",
					title: "Registration Successful",
					text: "Thank you for registering!"

				})


				window.location.href = "/login";


			} else {

				Swal.fire({

					icon: "error",
					title: "Registration Failed",
					text: "Something went wrong."

				})
			}

		})
	}



	return (

		user.id
		?
		<Navigate to="/products" replace={true}/>
		:
		<>
			<h1 className="my-5 text-center">Register</h1>
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group>
					<Form.Label className="py-3">First Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={e => {setFirstName(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="py-3">Last Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={e => {setLastName(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="py-3">Email:</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="py-3">Mobile No:</Form.Label>
					<Form.Control type="number" placeholder="Enter 11 Digit No." required value={mobileNo} onChange={e => {setMobileNo(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="py-3">Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="py-3">Confirm Password:</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" required value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}}/>
				</Form.Group>
				{
					isActive
					? <Button className="my-5" variant="primary" type="submit">Submit</Button>
					: <Button className="my-5" variant="primary" disabled>Submit</Button>
				}
			</Form>
		</>

	)

}