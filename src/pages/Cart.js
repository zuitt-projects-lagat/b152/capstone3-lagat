//Import Components
import EmptyCart from "../components/EmptyCart"
import FilledCart from "../components/FilledCart"


export default function Cart(){


	let cartChecker = JSON.parse(localStorage.getItem("products"));

	return (

		<>
			<h1 className="my-5 text-center">Cart Items</h1>
			{
				cartChecker === null
				?
				<EmptyCart />
				:
				<FilledCart />
			}
		</>

	)

}