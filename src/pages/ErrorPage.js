//Import Components
//import Banner from '.../components/Banner'


export default function ErrorPage(){

	return(

		<>
			<h1 className="my-5">Page Not Found</h1>
			<p>The page you looking for does not exist.</p>
			<a href="/" className="btn btn-dark">Back to Home</a>
		</>

	)
}