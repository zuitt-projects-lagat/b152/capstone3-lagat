//Import react Components
import {useState, useContext} from 'react';

//Import react-bootstrap Components
import {Form, Button} from 'react-bootstrap';

//Import react-router-dom Components
import {Navigate} from 'react-router-dom';

//Import sweetalert2 Components
import Swal from 'sweetalert2';

//Import UserContext
import UserContext from '../userContext';


export default function AddProduct(){


	const {user} = useContext(UserContext);


	//Input States
	const [name,setName] = useState("");
	const [description,setDescription] = useState("");
	const [price,setPrice] = useState("");


	function createProduct(e){

		e.preventDefault();

		let token = localStorage.getItem('token');
		console.log(token);


		fetch('https://floating-waters-44436.herokuapp.com/products/',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price

			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data._id){
				Swal.fire({

					icon: "success",
					title: "Add Product Successful"
				})
				.then(function() {

				    		window.location.href = "/products";

				})
				
			} else {
				Swal.fire({

					icon: "errpr",
					title: "Add Product Failed",
					text: data.message

				})
			}
		})
	}



	return (
		
		user.isAdmin
		?
		<>
			<h1 className="my-5 text-center">Add Product</h1>
			<Form onSubmit={e => createProduct(e)}>
				<Form.Group>
					<Form.Label className="py-3">Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter Name" required value={name} onChange={e => {setName(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="py-3">Description:</Form.Label>
					<Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => {setDescription(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="py-3">Price:</Form.Label>
					<Form.Control type="number" placeholder="Enter Price" required value={price} onChange={e => {setPrice(e.target.value)}}/>
				</Form.Group>
				<Button variant="primary" type="submit" className="my-4">Submit</Button>
			</Form>
		</>
		:
		<Navigate to="/products" replace={true}/>

	)

}