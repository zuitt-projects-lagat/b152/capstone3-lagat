//Import react Components
import {useEffect, useContext} from 'react';

//Import react-router-dom Components
import {Navigate} from 'react-router-dom';

//Import UserContext
import UserContext from '../userContext';


export default function Logout(){


	//Destructuring UserContext
	const {setUser, unsetUser} = useContext(UserContext);


	//clear the localStorage
	unsetUser();


	useEffect(() => {

		setUser({

			id: null,
			isAdmin: null

		})

	},[setUser])



	return (

		<Navigate to="/login" replace={true} />

	)

}