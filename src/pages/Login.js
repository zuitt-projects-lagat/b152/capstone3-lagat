//Import react Components
import {useState, useContext, useEffect} from 'react';

//Import react-bootstrap Components
import {Form, Button} from 'react-bootstrap';

//Import react-router-dom Components
import {Navigate} from 'react-router-dom';

//Import sweetalert2 Components
import Swal from 'sweetalert2';

//Import UserContext
import UserContext from '../userContext';


export default function Login(){


	const {user,setUser} = useContext(UserContext);


	//Input States
	const [email,setEmail] = useState("");
	const [password,setPassword] = useState("");


	//Conditional Rendering for Button
	const [isActive,setIsActive] = useState(false);

	useEffect(()=>{

		if(email !== "" && password !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[email,password])



	function loginUser(e){

		e.preventDefault();

		fetch('https://floating-waters-44436.herokuapp.com/users/login',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				email: email,
				password: password

			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.accessToken){

				Swal.fire({

					icon:"success",
					title: "Login Successful",
					text: "Thank you for logging in!"

				})


				localStorage.setItem('token',data.accessToken);
				localStorage.setItem('products', 'null');

				let token = localStorage.getItem('token');
				console.log(token);


				fetch('https://floating-waters-44436.herokuapp.com/users/getUserDetails',{

					method: 'GET',
					headers: {
						'Authorization': `Bearer ${token}`
					}

				})
				.then(res => res.json())
				.then(data => {

					//console.log(data);

					setUser({

						id: data._id,
						isAdmin: data.isAdmin

					})

				})

			} else {

				Swal.fire({

					icon: "error",
					title: "Login Failed",
					text: data.message

				})
				
			}

		})
	}



	return (

		user.id 
		?
		<Navigate to="/products" replace={true} />
		:
		<>
			<h1 className="my-5 text-center">Login</h1>
			<Form onSubmit={e => loginUser(e)}>
				<Form.Group>
					<Form.Label className="py-1">Email:</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="py-3">Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}}/>
				</Form.Group>
				{
					isActive
					? <Button variant="primary" type="submit" className="my-4">Submit</Button>
					: <Button variant="primary" disabled className="my-4">Submit</Button>
				}
			</Form>
		</>

	)


}