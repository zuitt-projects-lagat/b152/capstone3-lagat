//import react bootstrap components
import {Row, Col} from 'react-bootstrap';
import Carousel from 'react-bootstrap/Carousel';
import img1 from '../images/img1.JPG';
import img2 from '../images/img2.JPG';
import img3 from '../images/img3.JPG';


export default function Home(){

	return (
		<>
			<Row className="heading">
				<Col className="text-center p-5">
					<h1 className="mb-3">nic.</h1>
					<h6 className="mb-3"><i>Perfume Shop</i></h6>
					<h4 className="mb-3">Be chic, be <b>nic.</b></h4>
					<a href="/products" className="btn btn-outline-secondary">Browse Fragrances</a>
				</Col>
			</Row>
			<Carousel>
			  <Carousel.Item>
			    <img
			      className="d-block w-100"
			      src={img1}
			      alt="First slide"
			    />
			  </Carousel.Item>
			  <Carousel.Item>
			    <img
			      className="d-block w-100"
			      src={img2}
			      alt="Second slide"
			    />
			  </Carousel.Item>
			  <Carousel.Item>
			    <img
			      className="d-block w-100"
			      src={img3}
			      alt="Third slide"
			    />
			  </Carousel.Item>
			</Carousel>
		</>

	)
}