//Import react Components
import {useState, useEffect, useContext} from 'react';

//Import react-bootstrap Components:
import {Card, Button, Row, Col} from 'react-bootstrap';

//Import react-router-dom Components
import {useParams, Link} from 'react-router-dom';

//Import sweetalert2 Components
import Swal from 'sweetalert2';

//Import UserContext
import UserContext from '../userContext';


export default function ViewProduct(){

	const {productId} = useParams();


	//Destructuring
	const {user} = useContext(UserContext);
	//console.log(user);

	const [quantity, setQuantity] = useState(1);
	const [productDetails, setProductDetails] = useState({

		name: null,
		description: null,
		price: null,
		image: null

	})

	useEffect(() => {

		fetch(`https://floating-waters-44436.herokuapp.com/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {

			setProductDetails({

				name: data.name,
				description: data.description,
				price: data.price,
				image: data.image

			})

		})

	},[productId])

	//console.log(productDetails.image);


	function addProduct(){


		let productList = JSON.parse(localStorage.getItem("products"));
		if(productList == null){

			productList = [];

		}

		let product = {

			productId: productId,
			name: productDetails.name,
			price: productDetails.price,
			quantity: quantity

		};


		productList.push(product);

		localStorage.setItem("products", JSON.stringify(productList));

		Swal.fire({

			icon:"success",
			title: "Added to Cart",
			text: "Thank you for shopping with us!"

		})
		.then(function() {

    		window.location.href = "/products";

		});

	}



	return (

		<Row className="mt-5">
			<Col>
				<Card>
					<Card.Body className="text-center">
						<Card.Title>{productDetails.name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{productDetails.description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>&#8369; {productDetails.price}.00</Card.Text>
						<Card.Subtitle className="pb-3">Quantity:</Card.Subtitle>
						<Button variant="dark" className="btn" size="sm" disabled={quantity === 1} onClick={() => setQuantity(quantity - 1)}>-</Button>
						<span>   {quantity}   </span>
						<Button variant="dark" className="btn" size="sm" disabled={quantity === 50} onClick={() => setQuantity(quantity + 1)}>+</Button>
					</Card.Body>
					{
						user.id && user.isAdmin === false
						? <Button variant="dark" className="btn-block" onClick={() => {addProduct()}}>Add to Cart</Button>
						: <Link className="btn btn-outline-danger btn-block" to="/login">Login To Purchase</Link>
					}
				</Card>

			</Col>
		</Row>

	)

}