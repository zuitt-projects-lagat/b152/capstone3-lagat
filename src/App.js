//Import react Components
import {useState, useEffect} from 'react';

//Import react-bootstrap Components
import {Container} from 'react-bootstrap';

//Import react-router-dom Components
import {BrowserRouter as Router} from 'react-router-dom';
import {Route,Routes} from 'react-router-dom'

//Import components
import AppNavBar from './components/AppNavBar';

//Import pages
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ViewProduct from './pages/ViewProduct';
import AddProduct from './pages/AddProduct';
import Cart from './pages/Cart';
import ErrorPage from './pages/ErrorPage';

//Import UserProvider from userContext
import {UserProvider} from './userContext';


export default function App(){


  const [user, setUser] = useState({

    id: null,
    isAdmin: null

  })


 useEffect(()=>{

    fetch('https://floating-waters-44436.herokuapp.com/users/getUserDetails',{

      method: 'GET',
      headers: {

        'Authorization': `Bearer ${localStorage.getItem('token')}`

      }

    })
    .then(res => res.json())
    .then(data => {

      setUser({

        id: data._id,
        isAdmin: data.isAdmin

      })

    })

  },[])

  const unsetUser = () => {

    localStorage.clear()

  }



  return (

    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavBar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/products" element={<Products />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/register" element={<Register />} />
              <Route path="products/viewProduct/:productId" element={<ViewProduct />} />
              <Route path="/addProduct" element={<AddProduct />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="/*" element={<ErrorPage />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>

  )
}