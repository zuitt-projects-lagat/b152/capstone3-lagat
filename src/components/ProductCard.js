//Import react-bootstrap Components
import {Col, Card} from 'react-bootstrap';

//Import react-router-dom Components
import {Link} from 'react-router-dom';


export default function ProductCard({productProp}){

	return (

	<Col xs={12} md={4}>
		<Card className="p-3 cardHighlight my-3">
			<Card.Img variant="top" src={require(`../images/${productProp.image}`)} />
			<Card.Body>
				<Card.Title>
						{productProp.name}
				</Card.Title>
				<Card.Text>
						{productProp.description}
				</Card.Text>
				<Card.Text>
						&#8369; {productProp.price}.00
				</Card.Text>
				<Link to={`/products/viewProduct/${productProp._id}`} className="btn btn-outline-secondary">View Product</Link>
			</Card.Body>
		</Card>
	</Col>

	)

}