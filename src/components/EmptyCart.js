export default function EmptyCart(){

	return (
		<>
			<h4 className="text-center">Your Cart is empty.</h4>
			<a href="/products" className="btn btn-dark mt-4">Browse Products</a>
		</>

	)
}