//Import react Components
import {useContext} from 'react';

//Import react-bootstrap Components:
import {Nav, Navbar, Container} from 'react-bootstrap'

//Import react-router-dom Components
import {Link} from 'react-router-dom';

//Import UserContext
import UserContext from '../userContext';


export default function AppNavBar(){


	//UserContext Destructuring
	const {user} = useContext(UserContext);



	return (

		<Navbar bg="dark" variant="dark" expand="lg">
			<Container fluid>
				<Navbar.Brand href="/">nic.</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						<Link to="/products" className="nav-link">Products</Link>
						{
							user.id
							?
								user.isAdmin
								?
								<>
									<Link to="/addProduct" className="nav-link">Add Product</Link>
									<Link to="/logout" className="nav-link">Logout</Link>
								</>
								:
								<>
									<Link to="/cart" className="nav-link">Cart</Link>
									<Link to="/logout" className="nav-link">Logout</Link>
								</>
							:
							<>
								<Link to="/login" className="nav-link">Login</Link>
								<Link to="/register" className="nav-link">Register</Link>
							</>
						}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>

	)

}