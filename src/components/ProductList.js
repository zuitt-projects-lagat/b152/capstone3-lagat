//Import react-bootstrap Components
import {Card} from 'react-bootstrap';


export default function ProductList({productProp}){

	return (

		<Card>
			<Card.Body>
				<Card.Title>
						{productProp.name}
				</Card.Title>
				<Card.Text>
						Quantity: {productProp.quantity}
				</Card.Text>
				<Card.Text>
						Price: &#8369; {productProp.price*productProp.quantity}.00
				</Card.Text>
			</Card.Body>
		</Card>

	)

}