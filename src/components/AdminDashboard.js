//Import react Components
import {useState, useContext, useEffect} from 'react';

//Import react-bootstrap Components
import {Table, Button} from 'react-bootstrap';

//Import UserContext
import UserContext from '../userContext';


export default function AdminDashboard(){


	//Destructuring User Context
	const {user} = useContext(UserContext);
	//console.log(user)


	//State for All Products
	const[allProducts, setAllProducts] = useState([]);


	//Archive Function
	function archive(productId){

		fetch(`https://floating-waters-44436.herokuapp.com/products/archive/${productId}`,{

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}

		})
		.then(res => res.json())
		.then(data => {

			//console.log(data);
			window.location.href = "/products";

		})

	}


	//Activate Function
	function activate(productId){

		fetch(`https://floating-waters-44436.herokuapp.com/products/activate/${productId}`,{

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}

		})
		.then(res => res.json())
		.then(data => {

			//console.log(data);
			window.location.href = "/products";

		})

	}

	useEffect(()=>{

		if(user.isAdmin){

			fetch('https://floating-waters-44436.herokuapp.com/products/',{

				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}

			})
			.then(res => res.json())
			.then(data => {

				//console.log(data);
				setAllProducts(data.map(product => {

					return (

							<tr key={product._id}>
								<td>{product._id}</td>
								<td>{product.name}</td>
								<td>&#8369; {product.price}.00</td>
								<td>{product.isActive ? "Active": "Inactive"}</td>
								<td>
									{
										product.isActive
										?
										<Button variant="outline-dark" className="mx-2" onClick={()=>{archive(product._id)}}>Archive
										</Button>
										:
										<Button variant="secondary" className="mx-2" onClick={()=>{activate(product._id)}}>
											Activate
										</Button>
									}
								</td>
							</tr>


						)

				}))
			})

		}

	},[user.isAdmin])

	return (
		<>
			<h1 className="my-5 text-center">Admin Dashboard</h1>
			<Table bordered>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
						{allProducts}
				</tbody>
			</Table>
		</>

	)

}