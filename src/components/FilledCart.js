//Import react Components
import {useEffect, useState} from 'react';

//Import react-bootstrap Components
import {Button} from 'react-bootstrap';

//Import sweetalert2 Components
import Swal from 'sweetalert2';

//Import Components
import ProductList from './ProductList'


export default function FilledCart(){


	let productList = JSON.parse(localStorage.getItem("products"));


	//Input States
	const [productsArray, setProductsArray] = useState([])


	useEffect(()=>{
		
		setProductsArray(productList.map(product => {

			return (

					<ProductList key={product.productId} productProp={product} />

				)

		}))



	},[productList])


	let products = []
	let totalAmount = 0;

	for (let i = 0; i < productList.length; i++){

		products.push({

			productId: productList[i].productId,
			quantity: productList[i].quantity

		})

		totalAmount += productList[i].quantity * productList[i].price

	}

	//console.log(products);
	//console.log(totalAmount);


	function createOrder(){


		fetch('https://floating-waters-44436.herokuapp.com/orders/', {

			method: 'POST',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`,
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				totalAmount: totalAmount,
				products: products

			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.message === "You have successfully created an order!"){

				Swal.fire({

					icon:"success",
					title: "Purchase Complete",
					text: "Your order has been shipped out."

				})
				.then(function() {

					localStorage.setItem('products', 'null')
    				window.location.href = "/products";

				});

			} else {

				Swal.fire({

					icon: "error",
					title: "Purchase Failed",
					text: "Something went wrong."

				})
			}

		})

	}



	return (

		<>
			{productsArray}
			<h4 className="my-3 text-center">Total: &#8369; {totalAmount}.00</h4>
			<Button className="mt-3" onClick={()=>{createOrder()}}>Checkout</Button>
		</>

	)

}